oc login -u developer -p developer
oc project cicd

# scale / Stop PODs
oc scale dc/gitlab --replicas=1
oc scale dc/sonarqube --replicas=1
oc scale dc/nexus3 --replicas=1
oc scale dc/jenkins --replicas=1
oc scale dc/sonarqube-db --replicas=1
