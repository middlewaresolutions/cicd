Project for Continuous Integration and Deployment.

Environment installed:

* Nexus v3
* SonarQube
* Gogs

### Install All in One ###

Docker installation: 
```
docker-compose.yml
```

Openshift installation: 
```
oc import docker-compose -f .\docker-compose.yml

```

### Jenkins ###

Install into Openshift:
```
oc new-app jenkins-persistent
```

