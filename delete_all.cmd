oc delete dc gogs -n cicd
oc delete services gogs -n cicd
oc delete route gogs -n cicd

oc delete dc jenkins -n cicd
oc delete services jenkins -n cicd
oc delete route jenkins -n cicd

oc delete dc sonarqube -n cicd
oc delete services sonarqube -n cicd
oc delete route sonarqube -n cicd

oc delete dc sonarqube-db -n cicd
oc delete services sonarqube-db -n cicd
oc delete route sonarqube-db -n cicd

oc delete dc nexus -n cicd
oc delete services nexus -n cicd
oc delete route nexus -n cicd

oc delete is jenkins
oc delete is gogs
oc delete is sonarqube
oc delete is mysql
oc delete is nexus

oc delete dc jenkins2 -n cicd
oc delete services jenkins2 -n cicd
oc delete route jenkins2 -n cicd