oc login -u developer -p developer
oc project cicd

# scale / Stop PODs
oc scale dc/gitlab --replicas=0
oc scale dc/sonarqube --replicas=0
oc scale dc/nexus3 --replicas=0
oc scale dc/jenkins --replicas=0
oc scale dc/sonarqube-db --replicas=0
