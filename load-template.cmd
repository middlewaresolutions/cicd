## import basic images
oc import-image sonatype/nexus3:latest --confirm
# oc import-image jenkins:latest --confirm
oc import-image sonarqube:alpine --confirm
oc import-image mysql:5 --confirm
oc import-image gogs/gogs:latest --confirm

## import specific builds
oc new-build . --name=jenkins --context-dir=jenkins/ --strategy=docker

# load template
oc process -f cicd.yaml  | oc create -f -