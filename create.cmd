oc login -u developer -p developer
oc new-project cicd

# add service account for host path
oc create serviceaccount cicd -n cicd

oc login -u system:admin
oc project cicd

# necessaire !
oc adm policy add-scc-to-user hostaccess developer

# add role for cicd
# oc adm policy add-scc-to-user anyuid -z default
oc adm policy add-scc-to-user anyuid -z cicd
oc adm policy add-scc-to-user hostmount-anyuid -z cicd -n cicd

# create quota and limit
oc create -f resources-quota.yml -n cicd
oc create -f resources-limits.json -n cicd

oc login -u developer -p developer

oc new-build . --name=jenkins --context-dir=jenkins/ --strategy=docker
oc start-build jenkins --from-dir jenkins

# removed from v3.6
# oc import docker-compose -f .\docker-compose-local.yml

# import images
# oc import-image jenkins --from=jenkins:latest --confirm
# oc import-image nexus3 --from=sonatype/nexus3:latest --confirm
# oc import-image gogs --from=gogs/gogs:latest --confirm
# oc import-image mysql:5 --from=mysql:5 --confirm
# oc import-image sonarqube:alpine --from=sonarqube:alpine --confirm

# recreate from exported
oc process -f cicd.yaml | oc create -f -

oc expose svc/jenkins
oc expose svc/nexus
oc expose svc/sonarqube
oc expose svc/gogs
